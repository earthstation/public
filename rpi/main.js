var request = require('request');
var trackingID;
var token;
var array_threads = new Array();
var fs = require('fs');
var config = JSON.parse(fs.readFileSync(__dirname + '/config.json', 'utf8'));
//Express

const express = require('express');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
var path = require('path');
const app = express();
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(cookieParser());

app.use(express.static(path.join(__dirname, 'public')));



app.get('/', function (req, res) {
    res.cookie('username', config.username);
    res.cookie('password', config.password);
    res.sendFile(path.join(__dirname + '/public/panel.html'));

})

app.post('/settings', function (req, res) {
    config = JSON.parse(fs.readFileSync(__dirname + '/config.json', 'utf8'));

    config.username = req.body.email;
    config.password = req.body.password;

    fs.writeFile(__dirname + '/config.json', JSON.stringify(config), function (err) {
        if (err) return console.log(err);
        console.log(JSON.stringify(config));
        console.log('writing to ' + config);
        res.cookie('username', config.username);
        res.cookie('password', config.password);
        res.sendFile(path.join(__dirname + '/public/panel.html'));
    });


});

app.post('/restart', function (req, res) {
    restartNode();
    res.sendFile(path.join(__dirname + '/public/panel.html'));


});

app.listen(80, err => (err ? console.log('Error happened', err) : console.log('Server is up')));

//Express end




init();

function init() {

    getToken(function (data) {
        clearThreads();

        getScheme(data)

        console.log(data);

    });


}

function clearThreads() {
    for (var a = 0; a < array_threads.length; a++) {
        clearInterval(array_threads[a]);
    }
}

function getToken(callback) {

    const options = {
        url: config.base_uri + '/api/auth/login',
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'

        },
        json: {
            username: config.username,
            password: config.password
        }
    };
    //console.log(options);
    request(options, function (error, response, body) {
        //console.log('response code = ' + JSON.stringify(response.statusCode));
        //console.log('----- response = ' + JSON.stringify(response));
        if (!error && response.statusCode == 200) {
            console.log('Got token success')

            token = response.headers['x-auth'];
            callback(response.headers['x-auth'])
        } else {
            console.log('login error with code = ' + response.statusCode)
            console.log('Please visit localhost panel to manage accounts')
            console.log(JSON.stringify(body))
        }
    });

}


/*
            Deprecated
            Incompatibile
            {
    "trackingData": {
        "trackingID": "4763ed92-95d1-487c-9927-62af0436b23e",
        "lastUpdated": "2019-05-26T14:24:27"
    },
    "blueprints": [
        {
            "transferName": "trans1",
            "storageName": "Underground Humidity",
            "mandatory": true,
            "frequency": "11:22:34",
            "icon": "fas fa-icon"
        }
    ]
}
            */
function getScheme(data, callback) {






    const options = {
        url: config.base_uri + '/api/scheme/getScheme',
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'X-Auth': data

        }
    };

    request(options, function (error, response, body) {

        body = JSON.parse(body)



        if (!error && response.statusCode == 200) {

            trackingID = body.trackingData.trackingID;
            if (body.blueprints.length > 0) {
                setThreads(body);
            } else {
                console.log('nothing to set')
            }


        } else {
            console.log('login error with code = ' + response.statusCode)
            console.log(JSON.stringify(body))
        }
    });





}

function setThreads(data) {

    var delay_array = new Array();

    for (var a = 0; a < data.blueprints.length; a++) {

        if (data.blueprints[a].transferName.indexOf('MODBUS') != -1) {


            var fetchedValue = getName(data.blueprints[a].transferName)

            while (true) {
                var gen_delay = convertTime(data.blueprints[a].frequency) + randomIntInt(500, 4000);
                if (!delay_array.includes(gen_delay)) {
                    break;
                }
            }




            delay_array.push(gen_delay);
            console.log(parseInt(a + 1) + ' - adding thread for ' + data.blueprints[a].transferName + ' at ' + gen_delay);
            var interval = setInterval(getModbus, gen_delay, data.blueprints[a], fetchedValue);
            //convertTime(data.blueprints[a].frequency)
            array_threads.push(interval);

        }

    }

}

function randomIntInt(low, high) {
    return Math.floor(Math.random() * (high - low + 1) + low)
}



var spawn = require("child_process").spawn;

function getValue(callback) {
    var process = spawn('python', ["./modbus.py", '']);
    process.stdout.on('data', function (data) {

        var str = JSON.parse(data);
        str = JSON.parse("[" + data + "]");
        //process.kill();

        callback(str[0]);



    });

}

function getModbus(sensor, value) {




    value = parseInt(value);

    getValue(function (result) {
        //console.log(result)


        var sensor_obj = {
            name: sensor.transferName,
            value: result[value - 1]
        };

        addNewRecord(sensor_obj);
    });







}

function getModbus2(sensor, value) {


    value = parseInt(value);








    var sensor_obj = {
        name: sensor.transferName,
        value: 4
    };

    addNewRecord(sensor_obj);




}

function addNewRecord(data) {


    console.log('going for ' + JSON.stringify(data));

    const options = {
        url: config.base_uri + '/api/data/addRecord',
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'X-Auth': token

        },
        json: {
            "trackingID": trackingID,
            "data": [data]
        }
    };
    //console.log('sending data = ' + JSON.stringify(options));

    request(options, function (error, response, body) {



        if (!error && response.statusCode == 200) {
            console.log('Got token success')
            if (body.status == 'SUCCESS') {
                console.log('sending data = ' + JSON.stringify(options.json.data[0]))
                console.log('added record')
                //callback(false)
            } else {
                console.log('--------------------------------')
                console.log('addRecord error with status error');
                console.log('Restarting');
                console.log('--------------------------------')
                init();
            }

        } else {
            console.log('--------------------------------')
            console.log('addRecord error with code = ' + response.statusCode)
            console.log(JSON.stringify(body))
            console.log('--------------------------------')
            init();
        }
    });



}

function addNewBluePrint(data, callback) {




    const options = {
        url: config.base_uri + '/api/scheme/addBlueprint',
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'X-Auth': token

        },
        json: data.body
    };

    request(options, function (error, response, body) {



        if (!error && response.statusCode == 200) {
            console.log('Got token success')
            if (body.status == 'SUCCESS') {
                console.log('added blueprint')
                callback(true)
            }

        } else {
            console.log('login error with code = ' + response.statusCode)
            console.log(JSON.stringify(body))
            callback(false)
        }
    });





}

function convertTime(str) {
    var hms = str;
    var a = hms.split(':');

    var seconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]);
    return seconds * 1000;
}

function getName(str) {
    str = str.split(' ');
    loop1: for (var a = 0; a < str.length; a++) {
        if (str[a].indexOf('MODBUS') != -1) {
            return str[a].split('_')[2];
            break loop1;
        }
    }
}

function restartNode() {
    init();
    config = JSON.parse(fs.readFileSync(__dirname + '/config.json', 'utf8'));

}
