
echo '#####################################'
echo 'ESM Install script'
echo 'Step 1'
echo 'Updating PI'
echo '#####################################'
sudo apt-get update
echo '----Update complete----'

sudo pip install  -U pymodbus

echo '----Downloading Nodejs----'
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs
node -v
echo '----Nodejs Completed----'
echo '----Starting Code fetch----'
cd /home/pi
sudo mkdir esm
cd /home/pi/esm


sudo git clone https://earthstation@bitbucket.org/earthstation/public.git

echo '----Code fetch Completed----'
sudo chown pi /home/pi/esm/*

echo 'Installing libraries'

sudo npm install request -g
sudo npm install pm2 -g
sudo npm install express -g
sudo npm install morgan -g
sudo npm install cookie-parser -g
sudo npm install body-parser -g

echo 'Configuring startup'
cd /home/pi/esm/public/rpi
sudo npm install
pm2 start main.js
pm2 status