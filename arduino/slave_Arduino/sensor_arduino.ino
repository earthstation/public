#include <SPI.h>
#include <string.h>
#include "nRF24L01.h"
#include "RF24.h"

boolean myrelay=false;

RF24 radio(9, 10);
//TEMP
#include "printf.h"

//SENSORS

////////////////
#include <DHT.h>
#define DHTPIN 2     // what pin we're connected to
#define DHTTYPE DHT22   // DHT 22  (AM2302)
DHT dht(DHTPIN, DHTTYPE);
int chk;
double hum;  //Stores humidity value
double temp; //Stores temperature value

const int sensorPin= 0; 
int liquid_level;

//Relays
int relay_pin = 4;


  String final_result="";

void setup()
{

  Serial.begin(115200);
  printf_begin();
   pinMode(sensorPin, INPUT);
  dht.begin();
 pinMode(relay_pin,OUTPUT);
 digitalWrite(relay_pin,LOW);
  
  radio.begin();
  
  radio.setPayloadSize(32);
   radio.setChannel(0x76);
   radio.setDataRate(RF24_1MBPS);
     radio.setPALevel(RF24_PA_LOW);
 
  radio.openWritingPipe(0xf);
  const uint64_t pipe = (0xd);
  radio.openReadingPipe(1, pipe);
  radio.enableDynamicPayloads();
  radio.powerUp();
  //radio.startListening();
   radio.printDetails();
}
char receivedMessage[32] = {0};
void loop()
{
 unsigned long currentMillis = millis();

 if(Serial.available())
 {
  char a = Serial.read();
  if(a=='a')
  {
    Serial.println("Incoming command : a");
    
    
  }
 }

   radio.startListening();
    if(radio.available())
    {
        Serial.println("radio available");
       radio.read(receivedMessage, sizeof(receivedMessage));
  
    radio.stopListening();
    String stringMessage(receivedMessage);
        Serial.println(stringMessage);
    
   
    if(stringMessage=="GETSTRINGA")
    {
      
        
            
     final_result="read-A1-";
      Serial.println(final_result);
     int mylength=final_result.length()+1;
     
      char combined[mylength];
     final_result.toCharArray(combined,mylength);
       //delay(100);
      int mystatus=radio.write(combined, sizeof(combined));
      //Serial.print("sending status = ");
      //Serial.println(mystatus);
     
    }
else if(stringMessage=="relayon")
    {
      Serial.println("Turning RELAY ON");
      digitalWrite(relay_pin,HIGH);
    
    }
    else if(stringMessage=="relayoff")
    {
      Serial.println("Turning RELAY OFF");
      digitalWrite(relay_pin,LOW);
      
    
    }
    
    }
    

    
   

 
 
    //show_readings();
    
  
}


